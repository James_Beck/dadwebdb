﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DadWebDB.Models
{
    public class DadModel
    {
        public int ID { get; set; }
        public string Like { get; set; }
        public string Why { get; set; }
    }
}
