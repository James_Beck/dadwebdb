﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DadWebDB.Models;

namespace DadWebDB.Data
{
    public static class DbInitialize
    {
        public static void Initialize(DadModelContext context)
        {
            context.Database.EnsureCreated();

            if (context.Dad.Any())
            {
                return;
            }

            var dads = new DadModel[]
            {
                new DadModel { Like="Rotis and Curry", Why="Cause its food" },
                new DadModel { Like="Hiking in the back country", Why="Its so peaceful and scenic" },
                new DadModel { Like="Seeing people do well in their training and all that sort of stuff", Why="its very rewarding, I feeling like you're helping people achieve stuff" },
                new DadModel { Like="Family", Why="Really, really rewarding to watch people develop and turn into programmers and artists" },
            };

            foreach (DadModel d in dads)
            {
                context.Dad.Add(d);
            }

            context.SaveChanges();
        }
    }
}
