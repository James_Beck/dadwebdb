﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DadWebDB.Models;
using Microsoft.EntityFrameworkCore;

namespace DadWebDB.Data
{
    public class DadModelContext : DbContext
    {
        public DadModelContext(DbContextOptions<DadModelContext> options) : base(options)
        { }

        public DbSet<DadModel> Dad { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DadModel>().ToTable("DadModel");
        }
    }
}
