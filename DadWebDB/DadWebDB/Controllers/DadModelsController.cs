﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DadWebDB.Data;
using DadWebDB.Models;

namespace DadWebDB.Controllers
{
    public class DadModelsController : Controller
    {
        private readonly DadModelContext _context;

        public DadModelsController(DadModelContext context)
        {
            _context = context;
        }

        // GET: DadModels
        public async Task<IActionResult> Index()
        {
            return View(await _context.Dad.ToListAsync());
        }

        // GET: DadModels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dadModel = await _context.Dad
                .SingleOrDefaultAsync(m => m.ID == id);
            if (dadModel == null)
            {
                return NotFound();
            }

            return View(dadModel);
        }

        // GET: DadModels/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DadModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Like,Why")] DadModel dadModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(dadModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(dadModel);
        }

        // GET: DadModels/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dadModel = await _context.Dad.SingleOrDefaultAsync(m => m.ID == id);
            if (dadModel == null)
            {
                return NotFound();
            }
            return View(dadModel);
        }

        // POST: DadModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Like,Why")] DadModel dadModel)
        {
            if (id != dadModel.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dadModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DadModelExists(dadModel.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(dadModel);
        }

        // GET: DadModels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dadModel = await _context.Dad
                .SingleOrDefaultAsync(m => m.ID == id);
            if (dadModel == null)
            {
                return NotFound();
            }

            return View(dadModel);
        }

        // POST: DadModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var dadModel = await _context.Dad.SingleOrDefaultAsync(m => m.ID == id);
            _context.Dad.Remove(dadModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DadModelExists(int id)
        {
            return _context.Dad.Any(e => e.ID == id);
        }
    }
}
